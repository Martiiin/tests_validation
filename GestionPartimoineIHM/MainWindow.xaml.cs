﻿using GestionPatrimoineClasse;
using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasse.Enumeration;
using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Fabriques;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GestionPartimoineIHM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCalculer_Click(object sender, RoutedEventArgs e)
        {
            Credit credit = CreerCreditViaLesInput();
            AfficherDetailCredit(credit);
        }

        private Credit CreerCreditViaLesInput()
        {
            string tauxTypeNom = ((ComboBoxItem)cboTaux.SelectedItem).Content.ToString();
            ETypeTauxInteret typeTaux = ETypeTauxInteret.BonTaux;
            switch (tauxTypeNom)
            {
                case "Bon taux":
                    typeTaux = ETypeTauxInteret.BonTaux;
                    break;
                case "Tres bon taux":
                    typeTaux = ETypeTauxInteret.TresBonTaux;
                    break;
                case "Excellent taux":
                    typeTaux = ETypeTauxInteret.ExcellentTaux;
                    break;
            }

            int montantValeur = Int32.Parse(txtMontant.Text);
            int dureeValeur = Int32.Parse(txtDuree.Text);

            bool estFumeur = (ckbFumeur.IsChecked == true);
            bool estSportif = (ckbSportif.IsChecked == true);
            bool estCardiaque = (ckbCardiaque.IsChecked == true);
            bool estInformatique = (ckbInformatique.IsChecked == true);
            bool estPilote = (ckbPilote.IsChecked == true);

            Risques risques = new Risques(estSportif, estFumeur, estCardiaque, estInformatique, estPilote);
            IMontant montant = new MontantImmobilier(montantValeur);
            IDuree duree = new DureeImmobilier(dureeValeur);
            ITauxInteret tauxInteret = TauxInteretFabrique.GetTauxInteret(typeTaux, duree);
            ITauxAssurance tauxAssurance = TauxAssuranceFabrique.GetTauxAssurance(risques);

            return new Credit(duree, montant, tauxAssurance, tauxInteret);
        }

        private void AfficherDetailCredit(Credit credit)
        {
            txtPrixGlobalMensualite.Text = Math.Round(credit.GetPrixGlobalMensualite()).ToString();
            txtCotisationMensuelleAssurance.Text = Math.Round(credit.GetMontantCotisationMensuelAssurance()).ToString();
            txtTotalInteret.Text = Math.Round(credit.GetMontantTotalInteretsRembourses()).ToString();
            txtTotalAssurance.Text = Math.Round(credit.GetMontantTotalAssurance()).ToString();
            txtCapitalDixAns.Text = Math.Round(credit.GetMontantCapitalRemboursesAuBoutDeAnnee(10)).ToString();
        }
    }
}
