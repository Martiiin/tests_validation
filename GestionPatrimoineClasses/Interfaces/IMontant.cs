﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse.Interfaces
{
    public interface IMontant
    {
        public float Montant { get; set; }
    }
}
