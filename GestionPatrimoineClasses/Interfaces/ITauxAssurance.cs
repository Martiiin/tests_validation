﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse.Interfaces
{
    public interface ITauxAssurance
    {
        public float TauxAssurance { get; set; }
    }
}
