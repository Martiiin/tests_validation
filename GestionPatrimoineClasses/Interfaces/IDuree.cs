﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse.Interfaces
{
    public interface IDuree
    {
        public int Duree { get; set; }
    }
}
