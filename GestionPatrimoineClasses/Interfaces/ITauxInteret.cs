﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse.Interfaces
{
    public interface ITauxInteret
    {
        public float TauxInteret { get; set; }
    }
}
