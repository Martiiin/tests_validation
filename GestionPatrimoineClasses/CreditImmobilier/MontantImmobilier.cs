﻿using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse.CreditImmobilier
{
    public class MontantImmobilier : IMontant
    {
        const int MONTANT_MINIMUM = 50000;
        private float _montant;

        public float Montant 
        { 
            get => _montant; 
            set
            {
                if (value <= MONTANT_MINIMUM)
                {
                    throw new MontantInvalideException("Le montant Immobilier est trop petit !");
                }

                _montant = value;
            }
        }

        public MontantImmobilier(float montant)
        {
            Montant = montant;
        }
    }
}
