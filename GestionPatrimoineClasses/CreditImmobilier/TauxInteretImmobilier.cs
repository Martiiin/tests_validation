﻿using GestionPatrimoineClasse.Enumeration;
using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace GestionPatrimoineClasse.CreditImmobilier
{
    public class TauxInteretImmobilier : ITauxInteret
    {
        private float _tauxInteret;

        public float TauxInteret 
        { 
            get => _tauxInteret;
            set
            {
                if (value < 0f)
                {
                    throw new TauxInteretInvalideException("Le taux d'interet est trop bas !");
                }
                else if (value > 100f)
                {
                    throw new TauxAssuranceInvalideException("Le taux d'interet est trop élevé !");
                }
                _tauxInteret = value;
            }
        }

        public TauxInteretImmobilier(float tauxInteret)
        {
            TauxInteret = tauxInteret;
        }
    }
}
