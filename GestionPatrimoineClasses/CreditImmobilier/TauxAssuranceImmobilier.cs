﻿using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse.CreditImmobilier
{
    public class TauxAssuranceImmobilier : ITauxAssurance
    {
        private float _tauxAssurance;

        public float TauxAssurance
        {
            get => _tauxAssurance;
            set
            {
                if(value < 0.10f)
                {
                    throw new TauxAssuranceInvalideException("Le taux d'assurance est trop bas !");
                }
                else if(value > 0.90f)
                {
                    throw new TauxAssuranceInvalideException("Le taux d'assurance est trop élevé !");
                }
                _tauxAssurance = value;
            }
        }

        public TauxAssuranceImmobilier(float tauxAssurance)
        {
            TauxAssurance = tauxAssurance;
        }

    }
}
