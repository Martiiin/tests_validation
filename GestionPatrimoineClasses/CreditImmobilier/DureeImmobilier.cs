﻿using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse.CreditImmobilier
{
    public class DureeImmobilier : IDuree
    {
        const int DUREE_MINIMUM = 9;
        const int DUREE_MAXIMUM = 25;
        private int _duree;

        public int Duree 
        {
            get => _duree;
            set 
            {
                if (value < DUREE_MINIMUM)
                {
                    throw new DureeInvalideException("La duree de l'emprunt est trop minime !");
                }
                else if(value > DUREE_MAXIMUM)
                {
                    throw new DureeInvalideException("La duree de l'emprunt est trop élevé !");
                }

                _duree = value;
            }
        }

        public DureeImmobilier(int duree)
        {
            Duree = duree;
        }
    }
}
