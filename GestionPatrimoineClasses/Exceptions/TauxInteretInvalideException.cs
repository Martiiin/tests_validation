﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoineClasses.Exceptions
{
    public class TauxInteretInvalideException : Exception
    {
        public TauxInteretInvalideException()
        {

        }

        public TauxInteretInvalideException(string message) : base(message)
        {

        }
    }
}
