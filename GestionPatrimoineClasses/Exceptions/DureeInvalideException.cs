﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoineClasses.Exceptions
{
    public class DureeInvalideException : Exception
    {
        public DureeInvalideException()
        {

        }

        public DureeInvalideException(string message) : base(message)
        {
            
        }
    }
}
