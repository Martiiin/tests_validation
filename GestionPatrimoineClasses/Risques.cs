﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse
{
    public class Risques
    {
        public bool EstSportif { get; set; }

        public bool EstFumeur { get; set; }

        public bool EstAtteintTroubleCardiaque { get; set; }

        public bool EstTravailleurCommeIngenieurInformatique { get; set; }

        public bool EstTravailleurCommePiloteDeChasse { get; set; }

        public Risques (bool _EstSportif, bool _EstFumeur, bool _EstAtteintTroubleCardiaque, bool _EstTravailleurCommeIngenieurInformatique, bool _EstTravailleurCommePiloteDeChasse)
        {
            EstSportif = _EstSportif;
            EstFumeur = _EstFumeur;
            EstAtteintTroubleCardiaque = _EstAtteintTroubleCardiaque;
            EstTravailleurCommeIngenieurInformatique = _EstTravailleurCommeIngenieurInformatique;
            EstTravailleurCommePiloteDeChasse = _EstTravailleurCommePiloteDeChasse;
        }
    }
}
