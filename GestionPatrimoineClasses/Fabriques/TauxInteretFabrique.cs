﻿using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasse.Enumeration;
using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoineClasses.Fabriques
{
    public static class TauxInteretFabrique
    {
        private static readonly Collection<int> DUREE_EMPRUNT = new Collection<int>() { 7, 10, 15, 20, 25 };

        private static readonly Collection<float> BON_TAUX_INTERET = new Collection<float>() { 0.62f, 0.67f, 0.85f, 1.04f, 1.27f };
        private static readonly Collection<float> TRES_BON_TAUX_INTERET = new Collection<float>() { 0.43f, 0.55f, 0.73f, 0.91f, 1.15f };
        private static readonly Collection<float> EXCELLENT_TAUX_INTERET = new Collection<float>() { 0.35f, 0.45f, 0.58f, 0.73f, 0.89f };

        public static ITauxInteret GetTauxInteret(ETypeTauxInteret eTypeTauxInteret, IDuree dureeInterface)
        {
            Collection<float> listeTauxInteret = new Collection<float>();

            switch (eTypeTauxInteret)
            {
                case ETypeTauxInteret.BonTaux:
                    listeTauxInteret = BON_TAUX_INTERET;
                    break;
                case ETypeTauxInteret.TresBonTaux:
                    listeTauxInteret = TRES_BON_TAUX_INTERET;
                    break;
                case ETypeTauxInteret.ExcellentTaux:
                    listeTauxInteret = EXCELLENT_TAUX_INTERET;
                    break;
                default:
                    throw new TauxInteretInvalideException("Le taux d'interet n'est pas correct");
            }

            int indexDureeEmprunt = GetIndexDureeEmprunt(dureeInterface.Duree);

            return new TauxInteretImmobilier(listeTauxInteret[indexDureeEmprunt]);
        }

        private static int GetIndexDureeEmprunt(int dureeEmprunt)
        {
            int indexDureeEmprunt = 0;

            for (int index = 0; index < DUREE_EMPRUNT.Count; index++)
            {
                if (DUREE_EMPRUNT[index] <= dureeEmprunt)
                {
                    indexDureeEmprunt = index;
                }
            }

            return indexDureeEmprunt;
        }
    }
}
