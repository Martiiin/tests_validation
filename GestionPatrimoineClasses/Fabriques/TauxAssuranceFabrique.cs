﻿using GestionPatrimoineClasse;
using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasse.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoineClasses.Fabriques
{
    public static class TauxAssuranceFabrique
    {
        const float TAUX_BASE = 0.3f;
        const float TAUX_FUMEUR = 0.15f;
        const float TAUX_SPORTIF = -0.05f;
        const float TAUX_CARDIAQUE = 0.3f;
        const float TAUX_INGENIEUR_INFORMATIQUE = -0.05f;
        const float TAUX_PILOTE_CHASSE = 0.15f;

        public static ITauxAssurance GetTauxAssurance(Risques risque)
        {
            float taux = TAUX_BASE;

            if (risque.EstFumeur)
            {
                taux += TAUX_FUMEUR;
            }
            if (risque.EstSportif)
            {
                taux += TAUX_SPORTIF;
            }
            if (risque.EstAtteintTroubleCardiaque)
            {
                taux += TAUX_CARDIAQUE;
            }
            if (risque.EstTravailleurCommeIngenieurInformatique)
            {
                taux += TAUX_INGENIEUR_INFORMATIQUE;
            }
            if (risque.EstTravailleurCommePiloteDeChasse)
            {
                taux += TAUX_PILOTE_CHASSE;
            }

            return new TauxAssuranceImmobilier(taux);
        }
    }
}
