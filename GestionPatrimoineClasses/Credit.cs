﻿using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Exceptions;
using System;

namespace GestionPatrimoineClasse
{
    public class Credit
    {
        private static readonly int MOIS_DANS_ANNEE = 12;

        private IDuree duree;
        private IMontant montant;
        private ITauxAssurance tauxAssurance;
        private ITauxInteret tauxInteret;

        public Credit(IDuree _duree, IMontant _montant, ITauxAssurance _tauxAssurance, ITauxInteret _tauxInteret)
        {
            duree = _duree;
            montant = _montant;
            tauxAssurance = _tauxAssurance;
            tauxInteret = _tauxInteret;
        }

        public float GetPrixGlobalMensualite()
        {
            return (float)Math.Round(GetRemboursementMensuel() + GetMontantCotisationMensuelAssurance());
        }

        public float GetRemboursementMensuel()
        {
            float numerateur = montant.Montant * (tauxInteret.TauxInteret / 100f / MOIS_DANS_ANNEE);
            float denominateur = 1 - (float)Math.Pow(1 + tauxInteret.TauxInteret / 100f / MOIS_DANS_ANNEE, -MOIS_DANS_ANNEE * duree.Duree);
            float resultat = numerateur / denominateur;
            return resultat;
        }

        public float GetMontantCotisationMensuelAssurance()
        {
            float resultat = montant.Montant * (tauxAssurance.TauxAssurance / 100f) / MOIS_DANS_ANNEE;
            return resultat;
        }

        public float GetMontantTotalInteretsRembourses()
        {
            return (float)Math.Round((MOIS_DANS_ANNEE * duree.Duree * GetRemboursementMensuel() - montant.Montant));
        }

        public float GetMontantTotalAssurance()
        {
            return GetMontantCotisationMensuelAssurance() * duree.Duree * MOIS_DANS_ANNEE;
        }

        public float GetMontantCapitalRemboursesAuBoutDeAnnee(int annee)
        {
            if (annee < 1 || annee > duree.Duree)
            {
                throw new DureeInvalideException($"L'annee de remboursement doit etre entre 1 et ${duree.Duree}");
            }
            return GetRemboursementMensuel() * annee * 12;
        }
    }
}
