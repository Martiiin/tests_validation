﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestionPatrimoineClasse.Enumeration
{
    public enum ETypeTauxInteret
    {
        BonTaux,
        TresBonTaux,
        ExcellentTaux
    }
}
