﻿using GestionPatrimoineClasse;
using GestionPatrimoineClasses.Fabriques;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GestionPatriomoineTests.Fabriques
{
    public class TauxAssuranceFabriqueDonnee
    {
        public static IEnumerable<object[]> Donnee
        {
            get
            {
                yield return new object[] { 0.6f, new Risques(false, false, true, false, false) };
                yield return new object[] { 0.3f, new Risques(false, false, false, false, false) };
                yield return new object[] { 0.8f, new Risques(true, true, true, true, true) };

            }
        }
    }

    public class TauxAssuranceFabriqueTest
    {
        [Theory]
        [MemberData(nameof(TauxAssuranceFabriqueDonnee.Donnee), MemberType = typeof(TauxAssuranceFabriqueDonnee))]
        public void GetBonneValeur(float _valeurVoulue, Risques _risques)
        {
            var tauxAssurance = TauxAssuranceFabrique.GetTauxAssurance(_risques);
            Assert.Equal(tauxAssurance.TauxAssurance, _valeurVoulue, 2);
        }
    }
}
