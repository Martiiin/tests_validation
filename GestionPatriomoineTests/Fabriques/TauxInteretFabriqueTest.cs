﻿using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasse.Enumeration;
using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Fabriques;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GestionPatriomoineTests.Fabriques
{
    public class TauxInteretFabriqueDonnee
    {
        public static IEnumerable<object[]> Donnee
        {
            get
            {
                yield return new object[] { 0.67f, ETypeTauxInteret.BonTaux, new DureeImmobilier(10) };
                yield return new object[] { 0.73f, ETypeTauxInteret.TresBonTaux, new DureeImmobilier(16) };
                yield return new object[] { 0.73f, ETypeTauxInteret.ExcellentTaux, new DureeImmobilier(24) };

            }
        }
    }

    public class TauxInteretFabriqueTest
    {
        [Theory]
        [MemberData(nameof(TauxInteretFabriqueDonnee.Donnee), MemberType = typeof(TauxInteretFabriqueDonnee))]
        public void GetBonneValeur(float _valeurVoulue, ETypeTauxInteret eTypeTauxInteret, DureeImmobilier dureeImmobilier)
        {
            var tauxInteret = TauxInteretFabrique.GetTauxInteret(eTypeTauxInteret, dureeImmobilier);
            Assert.Equal(tauxInteret.TauxInteret, _valeurVoulue, 2);
        }
    }
}
