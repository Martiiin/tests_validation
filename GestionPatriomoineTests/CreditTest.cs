﻿using GestionPatrimoineClasse;
using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasse.Enumeration;
using GestionPatrimoineClasse.Interfaces;
using GestionPatrimoineClasses.Fabriques;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GestionPatriomoineTests
{
    public class CreditTest
    {
        public static IEnumerable<object[]> Donnee
        {
            get
            {
                MontantImmobilier montant = new MontantImmobilier(175000);
                DureeImmobilier duree = new DureeImmobilier(25);
                ITauxInteret tauxInteret = TauxInteretFabrique.GetTauxInteret(ETypeTauxInteret.BonTaux, duree);
                Risques risques = new Risques(false, true, true, true, false);
                ITauxAssurance tauxAssurance = TauxAssuranceFabrique.GetTauxAssurance(risques);
                var credit = new Credit(duree, montant, tauxAssurance, tauxInteret);
                yield return new object[] { credit };
            }
        }

        [Theory]
        [MemberData(nameof(CreditTest.Donnee), MemberType = typeof(CreditTest))]
        public void PrixGlobalMensualite(Credit credit)
        {
            Assert.Equal(783, credit.GetPrixGlobalMensualite(), 0);
        }

        [Theory]
        [MemberData(nameof(CreditTest.Donnee), MemberType = typeof(CreditTest))]
        public void RemboursementMensuel(Credit credit)
        {
            Assert.Equal(681, credit.GetRemboursementMensuel(), 0);
        }

        [Theory]
        [MemberData(nameof(CreditTest.Donnee), MemberType = typeof(CreditTest))]
        public void MontantCotisationMensuelAssurance(Credit credit)
        {
            Assert.Equal(102, credit.GetMontantCotisationMensuelAssurance(), 0);
        }

        [Theory]
        [MemberData(nameof(CreditTest.Donnee), MemberType = typeof(CreditTest))]
        public void MontantTotalInteretsRembourses(Credit credit)
        {
            Assert.Equal(29340, credit.GetMontantTotalInteretsRembourses(), 0);
        }

        [Theory]
        [MemberData(nameof(CreditTest.Donnee), MemberType = typeof(CreditTest))]
        public void MontantTotalAssurance(Credit credit)
        {
            Assert.Equal(30625, credit.GetMontantTotalAssurance(), 0);
        }

        [Theory]
        [MemberData(nameof(CreditTest.Donnee), MemberType = typeof(CreditTest))]
        public void CapitalRembourseAuBoutDeDixAns(Credit credit)
        {
            Assert.Equal(81736, credit.GetMontantCapitalRemboursesAuBoutDeAnnee(10), 0);
        }


    }
}
