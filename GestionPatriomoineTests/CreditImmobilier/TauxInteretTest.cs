﻿using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasses.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GestionPatriomoineTests.CreditImmobilier
{
    public class TauxInteretTest
    {
        [Fact]
        public void ValeurMinimumTauxAssurance()
        {
            Assert.Throws<TauxInteretInvalideException>(() => new TauxInteretImmobilier(-0.5f));
        }

        [Fact]
        public void ValeurMaximumTauxAssurance()
        {
            Assert.Throws<TauxAssuranceInvalideException>(() => new TauxInteretImmobilier(100.1f));
        }
    }
}
