using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasses.Exceptions;
using System;
using Xunit;

namespace GestionPatriomoineTests.CreditImmobilier
{
    public class TauxAssuranceTest
    {
        [Fact]
        public void ValeurMinimumTauxAssurance()
        {
            Assert.Throws<TauxAssuranceInvalideException>(() => new TauxAssuranceImmobilier(0.05f));
        }

        [Fact]
        public void ValeurMaximumTauxAssurance()
        {
            Assert.Throws<TauxAssuranceInvalideException>(() => new TauxAssuranceImmobilier(1f));
        }
    }
}
