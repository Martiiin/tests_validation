﻿using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasses.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GestionPatriomoineTests.CreditImmobilier
{
    public class MontantTest
    {
        [Fact]
        public void ValeurMinimumMontant()
        {
            Assert.Throws<MontantInvalideException>(() => new MontantImmobilier(48000));
        }
    }
}
