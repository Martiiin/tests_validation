﻿using GestionPatrimoineClasse.CreditImmobilier;
using GestionPatrimoineClasses.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GestionPatriomoineTests.CreditImmobilier
{
    public class DureeTest
    {
        [Fact]
        public void ValeurMinimumDuree()
        {
            Assert.Throws<DureeInvalideException>(() => new DureeImmobilier(7));
        }

        [Fact]
        public void ValeurMaximumDuree()
        {
            Assert.Throws<DureeInvalideException>(() => new DureeImmobilier(27));
        }
    }
}
